### What is this repository for? ###
Grasp pipeline for multi-fingered grasp data collection and experiments.

### ROS package name ###
``grasp_pipeline``

### RA-L Paper Branch ###
ral_2018_real_exp is the experiment branch for the RA-L paper "Modeling Grasp Type Improves Learning-Based Grasp Planning". 

### Dependencies ###
Opencv 2.4.9    
PCL 1.7    
ROS Kinetic      
MoveIt!   

### How to run the experiments pipeline? ###
``roslaunch grasp_pipeline data_collection_servers.launch``     
``roslaunch grasp_pipeline data_collection_client.launch`` 

### Grasp Servers ###
Launch grasp servers such as object segmentation, Moveit!, data recorder and other necessary grasping components. 

### Grasp Client ###
The grasp client file `data_collection_client.py` requests necessary grasp ROS services such as object segmentation, grasp planning and grasp controling.    
`grasp_pgm_inf_client()` request the grasp type planner service for the RA-L paper. 


### Moveit! ###
https://bitbucket.org/robot-learning/lbr4_allegro_moveit_config

### gazebo2rviz ###
https://bitbucket.org/robot-learning/gazebo2rviz   

